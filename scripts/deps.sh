#!/usr/bin/env bash

set -euo pipefail


# deps
dnf install -y git make rpmdevtools rpmlint

rpmdev-setuptree

