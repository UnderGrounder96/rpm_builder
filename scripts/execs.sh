#!/usr/bin/env bash

set -euo pipefail


ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"


# execs
ln -sv ~/rpmbuild ${ROOT_DIR}/rpmbuild
cd ~/rpmbuild/SOURCES/


VERSION="$(cat ${ROOT_DIR}/VERSION)"
mkdir -vp demo-${VERSION}


cp -v ${ROOT_DIR}/* demo-${VERSION}/ || true
tar -czf demo-${VERSION}.tar.gz demo-${VERSION}/

rm -rvf demo-${VERSION}/


# spec template
tee ~/rpmbuild/SPECS/demo.spec <<EOF
Name:           demo
Version:        ${VERSION}
Release:        1%{?dist}
Summary:        A simple spec demo script
BuildArch:      noarch

License:        MIT
Source0:        %{name}-%{version}.tar.gz

Requires:       python3

%description
A demo RPM build

%prep
%autosetup


%install
mkdir -vp %{buildroot}/tmp/%{name}-%{version}/
install -DCv LICENSE main.py VERSION %{buildroot}/tmp/%{name}-%{version}/



%files
%license LICENSE
/tmp/%{name}-%{version}/
EOF
