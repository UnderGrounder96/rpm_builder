RPM_BUILD = ~/rpmbuild
RPM_BUILD_RPMS = $(RPM_BUILD)/RPMS
RPM_BUILD_SPECS = $(RPM_BUILD)/SPECS

RPM_DEMO_SPEC = $(RPM_BUILD_SPECS)/demo.spec
RPM = $(RPM_BUILD_RPMS)/noarch/demo*.rpm


all: clean check


build:
	echo "Assuming the spec file is present in $(RPM_DEMO_SPEC)"
	# ln -sfv $(shell pwd)/demo.spec $(RPM_DEMO_SPEC)
	# cd $(RPM_BUILD_SPECS) && rpmdev-newspec demo


install: build
	rpmlint $(RPM_DEMO_SPEC) && sleep 3s
	rpmbuild -ba $(RPM_DEMO_SPEC)


check: install
	rpm -ivh $(RPM)
	rpm -qli demo
	python3 /tmp/demo*/main.py
	cp -v $(RPM) $(shell pwd)/build/


clean:
	rm -rvf $(RPM_BUILD_RPMS)/*
