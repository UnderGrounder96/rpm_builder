# rpm_builder

## Why ?

This repo tries to demystify rpm build.

## How ?

```bash
docker-compose up -d
docker-compose logs -tf
```

## Docs

[Basic](https://www.redhat.com/sysadmin/create-rpm-package)

[Advanced](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/packaging_and_distributing_software/index)
