FROM fedora:38

WORKDIR /code

COPY scripts/deps.sh /tmp

RUN bash /tmp/deps.sh

COPY . .

RUN bash scripts/execs.sh

CMD [ "make", "-Bj2" ]
